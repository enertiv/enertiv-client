# README

Enertiv API Sample Client for accessing data via Session or OAuth based authentication using Python. The purpose is to provide clear instructions for installation, quick testing and integrating the client into a project.

# Quick Start

```bash
$ python enertiv_client/get_clients.py USER_NAME PASSWORD CLIENT_ID CLIENT_SECRET
# Returns a list of clients similar to https://app.enertiv.com/api/client/
```

# Using as a Dependency

## Installation

This package can be installed using pip.

```bash
$ pip install -e git+https://bitbucket.org/enertiv/enertiv-client.git#egg=enertiv-client
```

## Using in a Project

The enertiv_client only needs to be instantiated once to establish the session and retrieve a access token, which can then be reused. Access token management is handled automatically within the client.

Identifiers (UUIDs) for location/sublocation/equipment can be determined by reviewing the documentation at https://app.enertiv.com/docs

```python
#import EnertivOAuthClient class from module enertiv_client.enertiv_oauth_client
from enertiv_client.enertiv_oauth_client import EnertivOAuthClient

#instantiate object 'client' to create an authenticated session with Enertiv API
app_name = 'db'
server_user = ''  # Your user
server_password = ''  # Your password
server_protocol = 'https'
server_host = 'staging.enertiv.com'  # dev, staging or app
server_port = 443

client = EnertivTokenClient(app_name, server_user, server_password, server_protocol, server_host, server_port)


#Examples of making GET requests to different API endpoints
response1 = client.get("https://app.enertiv.com/api/client")
print(response1.json())

response2 = client.get("https://app.enertiv.com/api/location/553232d8-0f87-4cab-86de-263934829f7a/characteristic/")
print(response2.json())

response3 = client.get("https://app.enertiv.com/api/location/553232d8-0f87-4cab-86de-263934829f7a/temperature/?fromTime=2019-01-01T05%3A00%3A00.000Z&toTime=2019-02-01T05%3A00%3A00.000Z&interval=hour")
print(response3.json())
```

## curl equivalent

The python client achieves the equivalent as the following curl, in a convenient wrapper.

You can use the command line to test that your local configuration is working:
```
curl -X POST -d "grant_type=password&username=<user_name>&password=<password>" -u"<client_id>:<client_secret>" https://app.enertiv.com/o/token/
```
You should get a response that looks something like this:
```
{"access_token": "<your-access-token>", "scope": "read", "expires_in": 86399, "refresh_token": "<your-refresh-token>"}
```
Access the API

The only thing needed to make the OAuth2Authentication class work is to insert the access_token you've received in the Authorization request header.

The command line to test the authentication looks like:
```
curl -H "Authorization: Bearer <your-access-token>" https://app.enertiv.com/api/client/
```