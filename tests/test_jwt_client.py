import json
import logging
from datetime import datetime, timedelta
from unittest import TestCase
from unittest.mock import patch, Mock

import pytz
import requests
from requests.models import Response
from enertiv_client.enertiv_jwt_client import EnertivJWTAuthClient, JWTToken, CacheStorage, \
  LocMemCache

logger = logging.getLogger(__name__)


class TestJWTClient(TestCase):
  def _get_client(self, storage=None, host="enertiv-test-host", app_name='test-app', user='test'):
    TEST_SERVER_PROTOCOL = "http"
    TEST_SERVER_PORT = "8000"
    TEST_SERVER_PASSWORD = "test"

    test_client = EnertivJWTAuthClient(
      app_name,
      user,
      TEST_SERVER_PASSWORD,
      TEST_SERVER_PROTOCOL,
      host,
      TEST_SERVER_PORT,
      storage=storage,
      timeout=300)
    return test_client

  def get_datetime_str(self, expired=False):
    now = datetime.now(tz=pytz.timezone('America/New_York'))
    delta = timedelta(hours=5)

    if not expired:
      return (now + delta).isoformat()

    return (now - delta).isoformat()

  def get_token_data(self, access_token_expired=False, refresh_token_expired=False):
    """
    Generate token data dict
    Expiration validity is driven by access_token_expired and refresh_token_expired
    """
    access_token_expiration = self.get_datetime_str(expired=access_token_expired)
    refresh_token_expiration = self.get_datetime_str(expired=refresh_token_expired)

    return JWTToken.create_from_storage_data({
      "access_token": "eyJ0eXAiOiJKV1QiwwJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE5Njk5MDc3LCJqdGkiOiJjZGRjNGNjMmM1MjE0NzI2YWI3OGZiMTUxOTEwMjljOSIsInVzZXJfaWQiOjI0fQ.ITfqnyc2wMFlVHhJJxj_C3f54ZZ0z6i7gRhpARiTpMo",
      "refresh_token": "eyJ0eXAiOiJKV1QeeCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYxOTc4NTM1NywianRpIjoiYzk5MjZlMDY1NmZmNDhhMjllYjNmZWRhMzBlZDJjOTUiLCJ1c2VyX2lkIjoyNH0.AtiQ188ZzeBVh8IY8v3iYqHmnXQrWCDb2beqzLJZ_s8",
      "access_token_expiration": access_token_expiration,
      "refresh_token_expiration": refresh_token_expiration
    })

  def test_access_token_validator(self):
    """
    Make sure that we have access_token in jwt_token dict and access_token is not expired
    """
    jwt_token_valid = self.get_token_data(access_token_expired=False)
    jwt_token_expired = self.get_token_data(access_token_expired=True)
    jwt_token_expired_missing_token = self.get_token_data(access_token_expired=True)
    jwt_token_expired_missing_token.access_token = None
    jwt_token_expired_invalid_time = self.get_token_data(access_token_expired=True)
    jwt_token_expired_invalid_time.access_token_expiration = 'asdfg'

    self.assertFalse(jwt_token_expired.access_token_valid())
    self.assertFalse(jwt_token_expired_missing_token.access_token_valid())
    self.assertFalse(jwt_token_expired_invalid_time.access_token_valid())
    self.assertTrue(jwt_token_valid.access_token_valid())

  def test_refresh_token_validator(self):
    """
    Make sure that we have refresh_token in token_data dict and refresh_token is not expired
    """
    jwt_token_refresh_expired = self.get_token_data(refresh_token_expired=True)
    jwt_token_refresh_expired_missing_token = self.get_token_data(refresh_token_expired=True)
    jwt_token_refresh_expired_missing_token.refresh_token = None

    jwt_token_refresh_valid = self.get_token_data(access_token_expired=True)

    self.assertFalse(jwt_token_refresh_expired.refresh_token_valid())
    self.assertFalse(jwt_token_refresh_expired_missing_token.refresh_token_valid())
    self.assertTrue(jwt_token_refresh_valid.refresh_token_valid())

  @patch("enertiv_client.enertiv_jwt_client.requests.session")
  @patch("enertiv_client.enertiv_jwt_client.EnertivJWTAuthClient.get_token_data")
  def test_get_token_executed(self, get_token, session):
    """
    Test initial state when the client doesn't have token_data in attributes or in the storage
    """
    client = self._get_client()
    client.session = session

    resp = Response()
    resp._content = json.dumps({"hello": "world"}).encode('utf-8')
    resp.status_code = requests.codes['ok']
    session.request.return_value = resp

    response = client.get(client.base_url)

    self.assertEqual(200, response.status_code)

  @patch("enertiv_client.enertiv_jwt_client.requests.session")
  @patch("enertiv_client.enertiv_jwt_client.EnertivJWTAuthClient.get_token_data")
  def test_token_data_exists_and_valid(self, get_token, session):
    """
    Token data is valid
    Expectations:
        get request should be executed
        Authorization is in headers and correct
    """
    client = self._get_client()
    client.session = session
    client.session.set(client.server_user, self.get_token_data())

    def mocked_request(*args, **kwargs):
      response = Response()
      response.headers.update(**kwargs.get('headers', {}))

      if args[0] == 'GET' and args[1] == client.base_url:
        response._content = {"key1": "value1"}
        response.status_code = 200

        return response

      response.status_code = 404
      return response

    session.request = Mock(side_effect=mocked_request)

    response = client.get(client.base_url)
    self.assertEqual(200, response.status_code)

  @patch("enertiv_client.enertiv_jwt_client.requests.session")
  @patch("enertiv_client.enertiv_jwt_client.EnertivJWTAuthClient.get_token_data")
  def test_token_data_exists_expiration_corrupted(self, get_token, session):
    """
    Token data is valid
    Expectations:
        get request should be executed
        Authorization is in headers and correct
    """
    client = self._get_client()
    client.session = session
    token_data = self.get_token_data()

    token_data.access_token_expiration = "asdasd"
    token_data.refresh_token_expiration = None

    client.store_token(token_data)

    def mocked_request(*args, **kwargs):
      response = Response()
      response.headers.update(**kwargs.get('headers', {}))

      if args[0] == 'GET' and args[1] == client.base_url:
        response._content = {"key1": "value1"}
        response.status_code = 200

        return response

      response.status_code = 404
      return response

    session.request = Mock(side_effect=mocked_request)

    response = client.get(client.base_url)
    self.assertEqual(200, response.status_code)

  @patch("enertiv_client.enertiv_jwt_client.EnertivJWTAuthClient.get_refresh_token_data")
  @patch("enertiv_client.enertiv_jwt_client.requests.session")
  @patch("enertiv_client.enertiv_jwt_client.EnertivJWTAuthClient.get_token_data")
  def test_access_token_expired_refresh_token_valid(self, get_token, session, refresh_tokens):
    """
    Token data exists
    Access Token is expired
    Refresh Token is valid
    Expectation: refresh_tokens will be executed
    """
    client = self._get_client()
    client.session = session
    client.store_token(self.get_token_data(access_token_expired=True))

    client.get(client.base_url).json()
    refresh_tokens.assert_called()

  @patch("enertiv_client.enertiv_jwt_client.EnertivJWTAuthClient.login")
  @patch("enertiv_client.enertiv_jwt_client.EnertivJWTAuthClient.delete_jwt_token")
  @patch("enertiv_client.enertiv_jwt_client.requests.session")
  @patch("enertiv_client.enertiv_jwt_client.EnertivJWTAuthClient.get_token_data")
  def test_get_unauthorized_in_response(self, get_token, session, delete_jwt_token, login):
    """
    Test when response is not equal to 200
    Expectation: delete token should be executed
    """
    client = self._get_client()
    client.session = session

    resp = Response()
    resp._content = json.dumps({"hello": "world"}).encode('utf-8')
    resp.status_code = requests.codes['unauthorized']
    session.request.return_value = resp

    response = client.get(client.base_url)
    delete_jwt_token.assert_called()
    self.assertEqual(response.status_code, requests.codes['unauthorized'])
    self.assertEqual(login.call_count, 2)

  @patch("enertiv_client.enertiv_jwt_client.EnertivJWTAuthClient.login")
  @patch("enertiv_client.enertiv_jwt_client.requests.session")
  @patch("enertiv_client.enertiv_jwt_client.EnertivJWTAuthClient.get_token_data")
  def test_get_forbidden_in_response(self, get_token, session, login):
    """
    Test forbidden in response

    Expectation:
        login method should be executed twice
    """
    client = self._get_client()
    client.session = session
    client.store_token(self.get_token_data(access_token_expired=True))

    def mocked_request(*args, **kwargs):
      response = Response()
      response.headers.update(**kwargs.get('headers', {}))

      if args[0] == 'GET' and args[1] == client.base_url:
        response._content = ""
        response.status_code = requests.codes['forbidden']

        return response

      response.status_code = 404
      return response

    session.request = Mock(side_effect=mocked_request)
    response = client.get(client.base_url)
    self.assertEqual(response.status_code, requests.codes['forbidden'])
    self.assertEqual(login.call_count, 1)

  def test_shared_tokens(self):
    """ Test that a token is stored in one client is able to be retrieved by another """
    storage = CacheStorage(cache=LocMemCache())
    client_1 = self._get_client(storage=storage)
    client_2 = self._get_client(storage=storage)
    self.assertIsNone(client_2.retrieve_token())
    client_1.store_token(self.get_token_data(access_token_expired=True))
    self.assertIsNotNone(client_2.retrieve_token())

  def test_not_shared_tokens(self):
    """Test that different storages are not shared """
    storage_1 = CacheStorage(cache=LocMemCache())
    storage_2 = CacheStorage(cache=LocMemCache())
    client_1 = self._get_client(storage=storage_1)
    client_2 = self._get_client(storage=storage_2)
    self.assertIsNone(client_2.retrieve_token())
    client_1.store_token(self.get_token_data(access_token_expired=True))
    self.assertIsNone(client_2.retrieve_token())

  def test_separate_users(self):
    """ Test that user and app name lead to different storage keys """
    storage = CacheStorage(cache=LocMemCache())
    client_1 = self._get_client(storage=storage)
    client_2 = self._get_client(storage=storage)
    client_3 = self._get_client(storage=storage, app_name='different-app')
    client_4 = self._get_client(storage=storage, user='different-user')
    client_5 = self._get_client(storage=storage, user='different-user', app_name='different-app')
    client_6 = self._get_client(storage=storage, host='different_host', app_name='different-app')
    client_7 = self._get_client(storage=storage, host='different_host', user='different-user')
    client_8 = self._get_client(storage=storage, host='different_host', user='different-user',
                                app_name='different-app')
    jwt_token = self.get_token_data()
    self.assertIsNone(client_1.retrieve_token())

    client_1.store_token(jwt_token)
    self.assertIsNotNone(client_1.retrieve_token())
    self.assertIsNotNone(client_2.retrieve_token())
    self.assertIsNone(client_3.retrieve_token())
    self.assertIsNone(client_4.retrieve_token())
    self.assertIsNone(client_5.retrieve_token())
    self.assertIsNone(client_6.retrieve_token())
    self.assertIsNone(client_7.retrieve_token())
    self.assertIsNone(client_8.retrieve_token())

