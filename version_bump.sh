#!/usr/bin/env bash
#title           :version_bump.sh
#description     :Update git tag and project version automatically for this git project.
#author		     :Eric Blum
#date            :20170118
#version         :0.3
#usage		     :./version_bump.sh [major, minor, patch, undo] [push]
#notes           :local staged changes must be committed. Follows semantic versioning
#==============================================================================

# check for uncommitted changes in the git project
git diff --exit-code -b >/dev/null
if [ $? == 1 ]; then
    git diff --exit-code -b
    echo
    echo "There are uncommitted changes in your project. Please commit everything before using version_bump"
    exit 1
fi

git fetch
counts=($(git rev-list --left-right --count origin/master...@))

# check for un-pulled remote changes
if [ ${counts[0]} != 0 ]; then
    echo "There are un-pulled changes in the branch. use git pull before using version_bump"
    exit 1
fi

# check for arguments
if [[ $1 -eq 1 ]]; then
    echo "please specify bump type. options: [major, minor, patch] (MAJOR.MINOR.PATCH) or undo"
    exit 1
fi
# change this for other projects, make sure the init files and requirements files are pathed to correctly
program_folder_name="enertiv_client"

# increments the version number in __init__.py. for version X.Y.Z, patch = X.Y.Z+1, minor = X.Y+1.Z, major = X+1.Y.Z
#if [ $1 == bump ]; then
init_file=${program_folder_name}/__init__.py
init_version=$(sed -n '/__version__/p' ${program_folder_name}/__init__.py | sed 's/.*"\(.*\)".*/\1/')
vs=(${init_version//./ })
if [[ $1 == patch ]]; then
    (( vs[2]++ ))
elif [[ $1 == minor ]]; then
    (( vs[1]++ ))
    vs[2]=0
elif [[ $1 == major ]]; then
    (( vs[0]++ ))
    vs[1]=0
    vs[2]=0
elif [[ $1 == undo ]]; then
    # get commit id and check if the message matches version_bump before undoing
    currid=$(git rev-parse @{0})
    lastid=$(git rev-parse @{1})
    message=$(git log --format=%B -n 1 ${currid})
    if [[ ${message} == "Version Bump."*"old version: "*"new version: "* ]]; then
        tag=$(git tag --contains ${currid})
        git tag -d ${tag}
        git reset --hard ${lastid}
    else
        echo "Latest commit not a version_bump commit. Aborting"
        exit 1
    fi
    exit 0
elif [[ $1 == push ]]; then
    currid=$(git rev-parse @{0})
    lastid=$(git rev-parse @{1})
    message=$(git log --format=%B -n 1 ${currid})
    if [[ ${message} == "Version Bump."*"old version: "*"new version: "* ]]; then
        git push --follow-tags
        exit 0
    else
        echo "Latest commit not a version_bump commit. Aborting"
        exit 1
    fi
else
    echo "No bump type given. use [major, minor, patch] (MAJOR.MINOR.PATCH) or undo to undo a version_bump"
    exit 1
fi
new_version=$(IFS=. ; echo "${vs[*]}")
echo "Version bumped: $1. old version: $init_version. new version: $new_version"
# update init file with new version
sed -i -e "s/$init_version/$new_version/g" ./${init_file}
#fi

# commits the init file
git -c core.quotepath=false commit --only ${program_folder_name}/__init__.py -m "Version Bump." -m "old version: $init_version. new version: $new_version" >/dev/null

version="$(git describe --abbrev=0 --tags)"
version_number=${version:1}
# get version in init
init_version=$(sed -n '/__version__/p' ${program_folder_name}/__init__.py | sed 's/.*"\(.*\)".*/\1/')
tag="v$init_version"
# create a git tag if none exists
if [ "$version_number" != "$init_version" ]; then
    tags=($(git tag))
    [[ " ${tags[@]} " =~ (^| )"$tag"($| ) ]] && echo "Tag already exists: $tag" || (echo "Creating tag: $tag"; git tag "$tag" -a -m '')
else
    echo "tags match. Current tag: $tag"
fi

if [[ $2 -eq 1 ]]; then
    continue
elif [[ $2 == push ]]; then
    echo "pushing changes"
    git push --follow-tags
fi


exit 0