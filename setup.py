from setuptools import setup
import re


def get_property(prop):
    return re.search(r'{}\s*=\s*[\'"]([^\'"]*)[\'"]'.format(prop), open(project + '/__init__.py').read()).group(1)

project = 'enertiv_client'

setup(
    name='enertiv-client',
    packages=['enertiv_client'],
    version=get_property('__version__'),
    description='Enertiv API Sample Client for accessing data via Session or OAuth based authentication using Python.',
    author='Felix Lipov',
    author_email='felix@enertiv.com',
    url='https://bitbucket.org/enertiv/enertiv-client',
    test_suite='nose.collector',
    tests_require=['nose'],
    classifiers=[
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Topic :: Software Development :: Testing',
    ],
    install_requires=[
        'requests', 'pytz'
    ]
)
