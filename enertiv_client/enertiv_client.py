import json
import logging

import requests
from requests.cookies import CookieConflictError
from requests.exceptions import Timeout

try:
  requests.packages.urllib3.disable_warnings()
except:
  pass
  # only relevant for newer versions of request library and urllib3 1.9

REQUEST_CODES = requests.codes
FORBIDDEN = REQUEST_CODES['forbidden']

logger = logging.getLogger("enertiv_client")


class LoginException(Exception):
  pass


def enertiv_server_login(func):
  def wrapper(*arg, **kwargs):
    if arg[0].csrf_token is None:
      arg[0].login()
    try:
      res = func(*arg, **kwargs)
    except LoginException as e:
      logger.warning("Relogging in %s" % e)
      arg[0].session = requests.session()
      response = arg[0].login()
      if response.status_code != 200:
        logger.warning("Relogon failed. response %s %s" % (response.status_code, response.text))
      res = func(*arg, **kwargs)
    return res
  return wrapper


class EnertivClient:
  csrf_token = None
  timeout = 120

  def __init__(self, app_name, server_user, server_password,
               server_protocol, server_host, server_port):
    self.app_name = app_name
    self.server_user = server_user
    self.server_password = server_password
    self.server_protocol = server_protocol
    self.server_host = server_host
    self.server_port = server_port
    # 'api' login endpoint is lighter on valid requests
    self.login_url = "%s://%s:%s/%s/login/" % (
      self.server_protocol, self.server_host, self.server_port, self.app_name)
    self.logout_url = "%s://%s:%s/%s/login/" % (
      self.server_protocol, self.server_host, self.server_port, self.app_name)

    self.session = None

  def login(self):
    try:
      self.session = requests.session()
      self.session.stream = False
      self.session.get(self.login_url, verify=False, timeout=self.timeout)
      self.csrf_token = self.session.cookies['csrftoken']
      login_data = dict(username=self.server_user, password=self.server_password,
                        csrfmiddlewaretoken=self.csrf_token)
      headers = {"X-CSRFToken": self.csrf_token,
                 "Referer": "%s://%s" % (self.server_protocol, self.server_host)}
      response = self.session.post(self.login_url, data=login_data, headers=headers,
                                   timeout=self.timeout, verify=False)
      if response.status_code != 200:
        logger.info(
          "login result for %s: %s %s" % (self.login_url, response.status_code, response.text))
    except Timeout as e:
      logger.warning('Login for %s timed out: %s' % (self.login_url, e))
      raise e
    except Exception as e:
      logger.error(e)
      raise e
    return response

  def get(self, url, **kwargs):
    return self.request("GET", url, **kwargs)

  def delete(self, url):
    return self.request("DELETE", url)

  def post(self, url, data=None, **kwargs):
    return self.request("POST", url, data=data, files=kwargs.get('files'), json=kwargs.get('json'))

  def put(self, url, data=None, **kwargs):
    return self.request("PUT", url, data=data, json=kwargs.get('json'))

  def patch(self, url, data=None, **kwargs):
    return self.request("PATCH", url, data=data, json=kwargs.get('json'))

  @enertiv_server_login
  def request(self, method, url, data={}, files=None, json_data=None):
    post_values = json_data or data
    post_values['csrfmiddlewaretoken'] = self.csrf_token
    self._check_login()
    try:
      sessionid = self.session.cookies['sessionid']
    except KeyError:
      sessionid = None
      logger.error('No session ID in cookies. Login credentials likely invalid')
    except CookieConflictError as e:
      self.session.cookies.clear()
      logger.warning("CookieConflictError %s. Relogging in" % e)
      self.login()
      sessionid = self.session.cookies['sessionid']  # better be there by now
    self.session.cookies.clear()
    self.session.cookies['csrftoken'] = self.csrf_token
    self.session.cookies['sessionid'] = sessionid
    headers = {"X-CSRFToken": self.csrf_token,
               "Referer": "%s://%s" % (self.server_protocol, self.server_host)}
    try:
      response = self.session.request(method, url, data=data,
                                      files=files, json=json, headers=headers,
                                      timeout=self.timeout, verify=False)
    except Timeout as e:
      logger.warning('%s for %s timed out: %s' % (method, url, e))
      raise e

    if response.status_code == FORBIDDEN:
      raise LoginException("Forbidden status code received for %s on %s %s %s" % (
        url, method, response.status_code, response.text))
    response.content
    return response

  def _check_login(self):
    try:
      if self.session is None or not any(key.startswith('sessionid') for
                                         key in self.session.cookies.keys()):
        self.login()
    except CookieConflictError as e:
      self.session.cookies.clear()
      logger.warning("CookieConflictError %s. Relogging in" % e)
      self.session = None

      self.login()


class EnertivRestAuthClient:
  def __init__(self, app_name, server_user, server_password,
               server_protocol, server_host, server_port,
               verify=False, timeout=120):
    """base_url = protocol://host:port/"""
    self.app_name = app_name
    self.server_user = server_user
    self.server_password = server_password
    self.server_protocol = server_protocol
    self.server_host = server_host
    self.server_port = server_port
    self.base_url = "%s://%s:%s" % (self.server_protocol, self.server_host, self.server_port)
    self.api_url = "%s/api" % self.base_url
    self.login_url = "%s/rest-auth/login/" % self.api_url
    self.logout_url = "%s/rest-auth/logout/" % self.api_url
    self.verify = verify
    self.timeout = timeout
    self.session = None

  def login(self):
    if self.session is None:
      self.session = requests.session()
      self.session.stream = False
    login_data = dict(username=self.server_user, password=self.server_password)
    try:
      response = self.session.post(self.login_url, data=login_data, timeout=self.timeout, verify=self.verify)
      if response.status_code != 200:
        logger.warning("login result failed for %s: %s %s" %
                       (self.login_url, response.status_code, response.text))
      self.set_login_headers(response)
    except Timeout as e:
      logger.warning('Login for %s timed out: %s' % (self.login_url, e))
      raise
    except Exception as e:
      logger.error(e)
      raise
    return response

  def set_login_headers(self, response):
    try:
      data = response.json()
      if 'key' in data:
        token = response.json()['key']
        self.session.headers.update({"Authorization": "Token %s" % token})
      else:
        raise ValueError
    except (json.JSONDecodeError, ValueError):
      csrf_token = self.session.cookies['csrftoken']
      csrf_headers = {"X-CSRFToken": csrf_token,
                      "Referer": "%s://%s" % (self.server_protocol, self.server_host)}
      self.session.headers.update(csrf_headers)

  def get(self, url, **kwargs):
    return self.request("GET", url, **kwargs)

  def delete(self, url):
    return self.request("DELETE", url)

  def post(self, url, data=None, **kwargs):
    return self.request("POST", url, data=data, files=kwargs.get('files'), json=kwargs.get('json'))

  def put(self, url, data=None, **kwargs):
    return self.request("PUT", url, data=data, json=kwargs.get('json'))

  def patch(self, url, data=None, **kwargs):
    return self.request("PATCH", url, data=data, json=kwargs.get('json'))

  def request(self, method, url, *args, **kwargs):
    self._check_login()

    try:
      response = self._request(method, url, *args, **kwargs)
    except Timeout as e:
      logger.warning('%s for %s timed out: %s' % (method, url, e))
      raise e

    if response.status_code == FORBIDDEN:
      logger.debug("Forbidden status code received for %s on %s %s %s" % (
        url, method, response.status_code, response.text))
      self.session = None
      self.login()
      response = self._request(method, url, *args, **kwargs)
    return response

  def _request(self, method, url, *args, **kwargs):
    timeout = kwargs.pop('timeout', self.timeout)
    return self.session.request(method, url, *args, timeout=timeout, **kwargs)

  def _check_login(self):
    try:
      if self.session is None or not any(key.startswith('sessionid') for
                                         key in self.session.cookies.keys()):
        self.login()
    except CookieConflictError as e:
      logger.warning("CookieConflictError %s. Relogging in" % e)
      self.session = None
      self.login()


class EnertivTokenClient(EnertivRestAuthClient):
  def login(self):
    if self.session is None:
      self.session = requests.session()
    login_data = dict(username=self.server_user, password=self.server_password)
    try:
      response = self.session.post(self.login_url, data=login_data, timeout=self.timeout,
                                   verify=self.verify)
      if response.status_code != 200:
        logger.warning("login result failed for %s: %s %s" %
                       (self.login_url, response.status_code, response.text))
      else:
        token = response.json()['key']
        self.session.headers.update({"Authorization": "Token %s" % token})
    except Timeout as e:
      logger.warning('Login for %s timed out: %s' % (self.login_url, e))
      raise
    except Exception as e:
      logger.error(e)
      raise
    return response

  def _check_login(self):
    if self.session is None or 'Authorization' not in self.session.headers:
      self.login()
