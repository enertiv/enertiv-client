import abc
import logging
from datetime import datetime
from typing import Optional, Union

import pytz
import requests
from requests.exceptions import Timeout

from enertiv_client.enertiv_oauth_client import LoginException

logger = logging.getLogger(__name__)


class LocMemCache:
  """
  Local cache which stores information in dictionary.
  It should only be used only for testing purposes where
  persisting and sharing tokens is not desired.
  """

  def __init__(self):
    self._cache = {}

  def get(self, key):
    return self._cache.get(key)

  def set(self, key, value):
    self._cache[key] = value

  def delete(self, key):
    self._cache.pop(key, None)


class Storage(abc.ABC):

  @abc.abstractmethod
  def get(self, key: str):
    pass

  @abc.abstractmethod
  def set(self, key: str, value: Optional[Union[str, dict]]):
    pass

  @abc.abstractmethod
  def delete(self, key: str):
    pass


class CacheStorage(Storage):
  def __init__(self, cache):
    self.cache = cache

  def get(self, key):
    return self.cache.get(key)

  def set(self, key, value):
    self.cache.set(key, value)

  def delete(self, key):
    self.cache.delete(key)


class JWTToken:
  access_token = None
  refresh_token = None
  access_token_expiration = None
  refresh_token_expiration = None

  def __init__(self, access_token, refresh_token, access_token_expiration,
               refresh_token_expiration):
    self.set_token_data(access_token, refresh_token, access_token_expiration,
                        refresh_token_expiration)

  def set_token_data(self, access_token: str, refresh_token: str,
                     access_token_expiration: str, refresh_token_expiration: str):
    self.access_token = access_token
    self.refresh_token = refresh_token
    self.access_token_expiration = access_token_expiration
    self.refresh_token_expiration = refresh_token_expiration

  def update_token_data(self, access_token, access_token_expiration):
    self.access_token = access_token
    self.access_token_expiration = access_token_expiration

  def storage_data(self):
    return {
      'access_token': self.access_token,
      'refresh_token': self.refresh_token,
      'access_token_expiration': self.access_token_expiration,
      'refresh_token_expiration': self.refresh_token_expiration,
    }

  @classmethod
  def create_from_storage_data(cls, data: dict) -> 'JWTToken':
    return cls(**data)

  @classmethod
  def _get_timestamp_from_str(cls, dt: str) -> datetime:
    local_dt = datetime.strptime(dt, '%Y-%m-%dT%H:%M:%S.%f%z')
    utc_dt = local_dt.astimezone(pytz.utc)
    return utc_dt

  def get_current_time(self) -> datetime:
    return datetime.now(pytz.utc)

  def access_token_valid(self, current_time: datetime = None) -> bool:
    return self._token_valid(self.access_token, self.access_token_expiration, current_time)

  def refresh_token_valid(self, current_time: datetime = None) -> bool:
    return self._token_valid(self.refresh_token, self.refresh_token_expiration, current_time)

  def _token_valid(self, token, expiration_time, current_time=None):
    if not token:
      return False
    if self.token_expired(expiration_time, current_time=current_time):
      return False
    return True

  def token_expired(self, token_expiration: str, current_time: datetime = None) -> bool:
    if current_time is None:
      current_time = self.get_current_time()
    try:
      token_expiration_time = self._get_timestamp_from_str(token_expiration)
    except ValueError:
      logger.warning("Invalid expiration time found: %s", token_expiration)
      return True
    if token_expiration_time > current_time:
      return False
    else:
      return True


class EnertivJWTAuthClient:
  def __init__(self, app_name, server_user, server_password,
               server_protocol, server_host, server_port, storage=None,
               verify=True, timeout=120, ):
    self.app_name = app_name
    self.server_user = server_user
    self.server_password = server_password
    self.server_protocol = server_protocol
    self.server_host = server_host
    self.server_port = server_port
    self.base_url = "%s://%s:%s" % (self.server_protocol, self.server_host, self.server_port)
    self.api_url = "%s/api" % self.base_url
    self.token_url = "%s/rest-auth/token/" % self.api_url
    self.refresh_token_url = "%s/rest-auth/token/refresh/" % self.api_url
    self.verify = verify
    self.timeout = timeout
    self.session = requests.session()

    if storage is None:
      storage = CacheStorage(cache=LocMemCache())

    self.storage = storage

    self.jwt_token = None  # type: Optional[JWTToken]

  def _get_storage_key(self, server_user: str):
    return f'{self.server_host}_{self.app_name}_{server_user}'

  def retrieve_token(self) -> Optional[JWTToken]:
    """Get token from storage if exists"""
    storage_token_data = self.storage.get(self._get_storage_key(self.server_user))
    if not storage_token_data:
      return None
    return JWTToken.create_from_storage_data(storage_token_data)

  def store_token(self, jwt_token: JWTToken):
    """Store token data in storage"""
    data = jwt_token.storage_data()
    self.storage.set(self._get_storage_key(self.server_user), data)

  def wipe_token(self):
    """delete token data in storage"""
    self.storage.delete(self._get_storage_key(self.server_user))

  def process_token_data(self, data: dict) -> JWTToken:
    access_token = data.get("access_token")
    refresh_token = data.get("refresh_token")
    access_token_expiration = data.get('access_token_expiration')
    refresh_token_expiration = data.get("refresh_token_expiration")
    return JWTToken(access_token=access_token, refresh_token=refresh_token,
                    access_token_expiration=access_token_expiration,
                    refresh_token_expiration=refresh_token_expiration)

  def process_refresh_token_data(self, data: dict, jwt_token: JWTToken) -> JWTToken:
    # in refresh token data calls the key is access instead of access_token
    access_token = data.get("access")
    access_token_expiration = data.get('access_token_expiration')
    jwt_token.update_token_data(access_token, access_token_expiration)
    return jwt_token

  def update_jwt_token(self, jwt_token: JWTToken):
    """Set token_data and update storage"""
    self.jwt_token = jwt_token
    self.store_token(self.jwt_token)

  def delete_jwt_token(self):
    self.jwt_token = None
    self.clear_authorization()
    self.wipe_token()

  def login_payload(self) -> dict:
    login_payload = dict(
      username=self.server_user,
      password=self.server_password
    )
    return login_payload

  def refresh_payload(self, jwt_token: JWTToken) -> dict:
    refresh_payload = dict(
      refresh=jwt_token.refresh_token
    )
    return refresh_payload

  def set_authorization(self, jwt_token: JWTToken):
    self.session.headers.update({"Authorization": "Bearer %s" % jwt_token.access_token})

  def clear_authorization(self):
    self.session.headers.pop("Authorization", None)

  def get_token_data(self):
    self.clear_authorization()
    try:
      response = self.session.post(self.token_url, data=self.login_payload(), timeout=self.timeout,
                                   verify=self.verify)

      if response.status_code != 200:
        logger.warning("login result failed for %s: %s %s" %
                       (self.token_url, response.status_code, response.text))

      data = response.json()
      return data
    except Timeout as e:
      logger.warning('Login for %s timed out: %s' % (self.token_url, e))
      raise
    except Exception as e:
      logger.error(e)
      raise

  def get_refresh_token_data(self):
    jwt_token = self.jwt_token
    if not jwt_token:
      raise ValueError('Cannot refresh access token without a refresh token')

    # Remove Authorization header since it has invalid data
    # and refresh call can be forbidden by JWTAuthenticationMiddleware
    self.clear_authorization()
    logger.info("Refreshing access token")

    try:
      response = self.session.post(self.refresh_token_url, data=self.refresh_payload(jwt_token),
                                   timeout=self.timeout, verify=self.verify)
      if response.status_code != 200:
        logger.warning("Refresh token result failed for %s: %s %s" %
                       (self.refresh_token_url, response.status_code, response.text))
      data = response.json()
      return data
    except Timeout as e:
      logger.warning('Refresh token for %s timed out: %s' % (self.refresh_token_url, e))
      raise
    except Exception as e:
      logger.error(e)
      raise

  @classmethod
  def _get_timestamp_from_str(cls, dt: str) -> datetime:
    local_dt = datetime.strptime(dt, '%Y-%m-%dT%H:%M:%S.%f%z')
    utc_dt = local_dt.astimezone(pytz.utc)
    return utc_dt

  def login(self):
    """
    :return: True or False depending on result of token processing
    :rtype: bool
    """
    try:
      jwt_token = self.jwt_token
      if jwt_token is None:
        # first attempt to get token data from storage
        storage_jwt_token = self.retrieve_token()
        if storage_jwt_token is None:
          token_data = self.get_token_data()
          jwt_token = self.process_token_data(token_data)
          self.update_jwt_token(jwt_token)
        else:
          jwt_token = storage_jwt_token

      # Ensure the local jwt token is still valid
      if not jwt_token.access_token_valid():
        # Check if storage has an updated token
        storage_jwt_token = self.retrieve_token()
        if storage_jwt_token and storage_jwt_token.access_token_valid():
          jwt_token = storage_jwt_token
        elif jwt_token.refresh_token_valid():
          # Refresh the access token using the refresh token
          self.jwt_token = jwt_token
          refresh_data = self.get_refresh_token_data()
          jwt_token = self.process_refresh_token_data(refresh_data, jwt_token)
        else:
          # Retrieve new access and refresh tokens
          token_data = self.get_token_data()
          jwt_token = self.process_token_data(token_data)
        self.update_jwt_token(jwt_token)
      self.set_authorization(jwt_token)
      return True
    except Exception as e:
      logger.exception("Login failed {}".format(e))
      return False

  def get(self, url, **kwargs):
    return self.request("GET", url, **kwargs)

  def delete(self, url):
    return self.request("DELETE", url)

  def post(self, url, data=None, **kwargs):
    return self.request("POST", url, data=data, files=kwargs.get('files'), json=kwargs.get('json'))

  def put(self, url, data=None, **kwargs):
    return self.request("PUT", url, data=data, json=kwargs.get('json'))

  def patch(self, url, data=None, **kwargs):
    return self.request("PATCH", url, data=data, json=kwargs.get('json'))

  def request(self, method, url, *args, **kwargs):
    """Automatically retry unauthorized responses after regenerating jwt_token"""
    self.login()
    response = self._request(method, url, *args, **kwargs)
    if response.status_code == requests.codes['unauthorized']:
      logger.warning("Unauthorized response %s retrying.", response.text)
      self.delete_jwt_token()
      self.login()
      response = self._request(method, url, *args, **kwargs)
    return response

  def _request(self, method, url, *args, **kwargs):
    response = self.session.request(method, url, *args, **kwargs)
    return response
